



document.addEventListener('DOMContentLoaded', function () {
  const tabs = document.querySelectorAll('.tabs-title');
  const tabContents = document.querySelectorAll('.tabs-content li');

  tabs.forEach((tab, index) => {
    tab.addEventListener('click', function () {
      tabs.forEach((t) => t.classList.remove('active'));
      tabContents.forEach((content) => content.classList.remove('active'));

      tab.classList.add('active');
      tabContents[index].classList.add('active');
    });
  });
});